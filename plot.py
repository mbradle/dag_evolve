import numpy as np
import matplotlib.pyplot as plt
x = np.genfromtxt('out')

k = 1
for i in range(1, x.shape[1], 2):
    plt.plot( x[:,0], x[:,i], label='Path ' + str(k))
    k += 1

k = 1
for i in range(2, x.shape[1], 2):
    plt.plot( x[:,0], x[:,i], ':', label='Path ' + str(k) + '_a')
    k += 1

sum = []

for i in range(0, x.shape[0]):
    s = 0
    for j in range(1, x.shape[1], 2):
        s += x[i,j]
    sum.append(s)

sum1 = []

for i in range(0, x.shape[0]):
    s = 0
    for j in range(2, x.shape[1], 2):
        s += x[i,j]
    sum1.append(s)

plt.plot( x[:,0], sum, label = 'Total' )
plt.plot( x[:,0], sum1, ':', label = 'Total_a' )

plt.xlabel('$\\tau$')
plt.ylabel('$G(\\tau)$')
N = int((x.shape[1] - 1) / 2)
plt.title('N = {:d}'.format(N))
plt.ylim([0.,1.02 * np.max(sum)])
plt.legend(loc="center right")
plt.savefig('out.pdf')
